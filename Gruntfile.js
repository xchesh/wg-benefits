module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                options: {
                    sourceMap: true,
                    outputStyle: 'compact'
                },
                files: {
                    'public/css/discounts.css': 'public/css/src/app.scss',
                }
            }
        },
        concat: {
            options: {
                separator: '\n',
            },
            dist: {
                src: ['public/js/app/*.js', 'public/js/app/**/*.js', '!public/js/app/app.compiled.js'],
                dest: 'public/js/app/app.compiled.js',
            },
        },
        watch: {
            gruntfile: {
                files: 'Gruntfile.js',
                tasks: ['default'],
            },
            concat: {
                files: ['public/js/app/**/*.js', '!public/js/app/app.compiled.js'],
                tasks: ['js']
            },
            css: {
                files: 'public/css/src/*.scss',
                tasks: ['style'],
            },
        },
    });

    grunt.registerTask('default', ['watch', 'sass', 'concat']);
    grunt.registerTask('style', ['sass']);
    grunt.registerTask('js', ['concat']);

}
