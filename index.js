var express = require('express'),
    bodyParser = require('body-parser'),
    router = require('./libs/router'),
    mongoose = require('mongoose');

var app = express();

/* Connect to MongoDB */
mongoose.connect('mongodb://localhost:27017/discounts');

app.set('view engine', 'jade');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use('/', router);

app.listen(3000, function() {
    console.log('Start WOD!');
});
