var express = require('express'),
    Discount = require('../server/models/Discount'),
    Category = require('../server/models/Category');

var router = express.Router();

router.get('/', function(req, res) {
    res.render('index');
})
router.get('/views/:name', function(req, res) {
    var name = req.params.name;
    res.render('templates/' + name);
});

function parseToModel(model, data) {
    for (var p in model) {
        if (data[p] && p != '_id') {
            model[p] = data[p]
        }
    }
    return model;
}
router.route('/api/discounts')
    .post(function(req, res) {
        var discount = new Discount();
        discount = parseToModel(discount, req.body);
        discount.save(function(err) {
            if (err)
                res.send(err);

            res.json({
                message: 'discount created!'
            });
        });
    })
    .get(function(req, res) {
        Discount.find(function(err, discounts) {
            if (err)
                res.send(err);

            res.json(discounts);
        });
    });
router.route('/api/discounts/:discount_id')
    .get(function(req, res) {
        Discount.findById(req.params.discount_id, function(err, discount) {
            if (err)
                res.send(err);

            res.json(discount);
        });
    })
    .put(function(req, res) {
        Discount.findById(req.params.discount_id, function(err, discount) {
            if (err)
                res.send(err);

            discount = parseToModel(discount, req.body);
            discount.save(function(err) {
                if (err)
                    res.send(err);

                res.json({
                    message: 'discount updated!'
                });
            });

        });
    })
    .delete(function(req, res) {
        Discount.remove({
            _id: req.params.discount_id
        }, function(err, discount) {
            if (err)
                res.send(err);

            res.json({
                message: 'Successfully deleted'
            });
        });
    });
router.route('/api/categories')
    .post(function(req, res) {
        var category = new Category();
        parseToModel(category, req.body);
        category.save(function(err) {
            if (err)
                res.send(err);

            res.json({
                message: 'Category created!'
            });
        });
    })
    .get(function(req, res) {
        Category.find(function(err, categories) {
            if (err)
                res.send(err);

            res.json(categories);
        });
    });

router.route('/api/categories/:category_id')
    .get(function(req, res) {
        Category.findById(req.params.category_id, function(err, category) {
            if (err)
                res.send(err);

            res.json(category);
        });
    })
    .put(function(req, res) {
        Category.findById(req.params.category_id, function(err, category) {
            if (err)
                res.send(err);

            parseToModel(category, req.body);
            category.save(function(err) {
                if (err)
                    res.send(err);

                res.json({
                    message: 'Category updated!'
                });
            });

        });
    })
    .delete(function(req, res) {
        Category.remove({
            _id: req.params.category_id
        }, function(err, category) {
            if (err)
                res.send(err);

            res.json({
                message: 'Successfully deleted'
            });
        });
    });

module.exports = router;
