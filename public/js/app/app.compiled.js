angular.module('discounts', ['ngRoute', 'ngAnimate','ui.bootstrap', 'uiGmapgoogle-maps'])
    .config(Router)

    .service('DiscountService', DiscountService)
    .service('CategoryService', CategoryService)

    .controller('TreeController', TreeController)
    .controller('StartController', StartController)
    .controller('AdminController', AdminController)
    .controller('DiscountController', DiscountController)
    .controller('CategoryController', CategoryController)

    .run(Startup);

Router.$inject = ['$routeProvider'];

function Router($routeProvider) {
    $routeProvider.
    when('/', {
        templateUrl: '/views/app',
        controller: 'StartController',
        reloadOnSearch: false
    }).
    when('/admin', {
        templateUrl: '/views/admin_main',
        controller: 'AdminController',
        reloadOnSearch: false
    }).
    when('/admin/discounts/:id', {
        templateUrl: '/views/discount_view',
        controller: 'DiscountController',
        reloadOnSearch: false
    }).
    when('/admin/categories/:id', {
        templateUrl: '/views/category_view',
        controller: 'CategoryController',
        reloadOnSearch: false
    }).
    otherwise({
        redirectTo: '/',
        reloadOnSearch: false
    });
}

AdminController.$inject = ['$scope', '$q', 'DiscountService', 'CategoryService'];

function AdminController($scope, $q, DiscountService, CategoryService) {

    function init() {
        $q.all({
            discounts: DiscountService.getDiscounts(),
            categories: CategoryService.getCategories()
        }).then(setToScope);
    }

    function setToScope(res) {
        for (var p in res) {
            $scope[p] = res[p].data;
        }
    }
    init();
}

CategoryController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', '$uibModal', 'CategoryService'];

function CategoryController($scope, $rootScope, $routeParams, $location, $uibModal, CategoryService) {
    /* Params */
    $scope.editMode = false;
    $scope.category = {};

    /* HTML Actions */
    $scope.submit = submit;
    $scope.removeCategory = removeCategory;

    /* Actions */
    function submit() {
        $scope.editMode ? CategoryService.saveCategory($scope.category).then(callUpdated) : CategoryService.createCategory($scope.category).then(clearForm);
    }

    function removeCategory() {
        if (!$scope.editMode) return;
        $uibModal.open({
            templateUrl: '/views/confirm_modal',
            resolve: {
                message: function() {
                    return 'Вы уверены, что хотите удалить категорию?';
                }
            },
            controller: function ($scope, $uibModalInstance, message) {
                $scope.message = message;
            }
        }).result.then(function() {
            CategoryService.removeCategory($scope.category).then(clearForm);
        })
    }

    function clearForm() {
        $location.path('/admin');
        callUpdated();
    }

    function callUpdated(){
        $rootScope.$emit('updated');
    }
    /* Initialization */
    function init() {
        $scope.editMode = $routeParams.id != 'create';
        if ($scope.editMode) {
            CategoryService.getCategory($routeParams.id).then(function(res) {
                $scope.category = res.data;
            });
        }
    }
    init();
}

DiscountController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', '$uibModal', 'DiscountService', 'CategoryService'];

function DiscountController($scope, $rootScope, $routeParams, $location, $uibModal, DiscountService, CategoryService) {
    /* Params */
    $scope.editMode = false;
    $scope.discount = {};
    $scope.newPlace = {};
    $scope.map = {
            center: {
                latitude: 53.9019481,
                longitude: 27.5688651
            },
            zoom: 11
        }
        /* HTML Actions */
    $scope.submit = submit;
    $scope.removeDiscount = removeDiscount;
    $scope.createPlace = createPlace;
    $scope.removePlace = removePlace;

    /* Actions */
    function submit() {
        $scope.editMode ? DiscountService.saveDiscount($scope.discount).then(callUpdated) : DiscountService.createDiscount($scope.discount).then(clearForm);
    }

    function removeDiscount() {
        if (!$scope.editMode) return;
        $uibModal.open({
            templateUrl: '/views/confirm_modal',
            resolve: {
                message: function() {
                    return 'Вы уверены, что хотите удалить скидку?';
                }
            },
            controller: function($scope, $uibModalInstance, message) {
                $scope.message = message;
            }
        }).result.then(function() {
            DiscountService.removeDiscount($scope.discount).then(clearForm);
        })
    }

    function createPlace() {
        if (!$scope.newPlace.address || $scope.newPlace.address.length < 1) return;
        if (!$scope.discount.places)
            $scope.discount.places = [];

        $scope.discount.places.push(angular.copy($scope.newPlace));
        $scope.newPlace = {};
    }

    function removePlace(indx) {
        $scope.discount.places.splice(indx, 1);
    }

    function clearForm() {
        $location.path('/admin');
        callUpdated();
    }
    function callUpdated(){
        $rootScope.$emit('updated');
    }
    /* Initialization */
    function init() {
        $scope.editMode = $routeParams.id != 'create';
        if ($scope.editMode) {
            DiscountService.getDiscount($routeParams.id).then(function(res) {
                $scope.discount = res.data;
            });
        }
        CategoryService.getCategories().then(function(res) {
            $scope.categories = res.data;
        })
    }
    init();
}

StartController.$inject = ['$scope'];

function StartController($scope) {
    console.log('000000');
}

TreeController.$inject = ['$scope', '$rootScope', '$q', 'DiscountService', 'CategoryService'];

function TreeController($scope, $rootScope, $q, DiscountService, CategoryService) {

    $scope.toggleCategory = toggleCategory;

    $rootScope.$on('updated', init);

    function toggleCategory(e, category) {
        if (e) {
            e.stopPropagation();
            e.preventDefault();
        }
        category._collapsed = !category._collapsed;
    }

    function init() {
        $q.all({
            discounts: DiscountService.getDiscounts(),
            categories: CategoryService.getCategories()
        }).then(setToScope);
    }

    function setToScope(res) {
        for (var p in res) {
            $scope[p] = res[p].data;
        }
    }
    init();
}

Startup.$inject = ['$rootScope', '$location'];

function Startup($rootScope, $location) {
    $rootScope.isPath = function(route){
        return $location.path().indexOf(route) >= 0;
    }
    $rootScope.isActive = function(route) {
        return $location.path() == route;
    }
}

CategoryService.$inject = ['$http', '$q'];

function CategoryService($http, $q) {
    var service = this;
    service.getCategories = function() {
        return $http.get('/api/categories');
    }
    service.getCategory = function(id) {
        return $http.get('/api/categories/' + id);
    }
    service.createCategory = function(category) {
        return $http.post('/api/categories', category);
    }
    service.saveCategory = function(category) {
        return $http.put('/api/categories/' + category._id, category);
    }
    service.removeCategory = function(category) {
        return $http.delete('/api/categories/' + category._id);
    }
}

DiscountService.$inject = ['$http'];

function DiscountService($http) {
    var service = this;
    service.getDiscounts = function() {
        return $http.get('/api/discounts');
    };
    service.getDiscount = function(id) {
        return $http.get('/api/discounts/' + id);
    }
    service.createDiscount = function(discount) {
        return $http.post('/api/discounts', discount);
    }
    service.saveDiscount = function(discount) {
        return $http.put('/api/discounts/' + discount._id, discount);
    };
    service.removeDiscount = function(discount) {
        return $http.delete('/api/discounts/' + discount._id);
    };
}
