angular.module('discounts', ['ngRoute', 'ngAnimate','ui.bootstrap', 'uiGmapgoogle-maps'])
    .config(Router)

    .service('DiscountService', DiscountService)
    .service('CategoryService', CategoryService)

    .controller('TreeController', TreeController)
    .controller('StartController', StartController)
    .controller('AdminController', AdminController)
    .controller('DiscountController', DiscountController)
    .controller('CategoryController', CategoryController)

    .run(Startup);
