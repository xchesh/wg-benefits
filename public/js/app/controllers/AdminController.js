AdminController.$inject = ['$scope', '$q', 'DiscountService', 'CategoryService'];

function AdminController($scope, $q, DiscountService, CategoryService) {

    function init() {
        $q.all({
            discounts: DiscountService.getDiscounts(),
            categories: CategoryService.getCategories()
        }).then(setToScope);
    }

    function setToScope(res) {
        for (var p in res) {
            $scope[p] = res[p].data;
        }
    }
    init();
}
