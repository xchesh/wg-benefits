CategoryController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', '$uibModal', 'CategoryService'];

function CategoryController($scope, $rootScope, $routeParams, $location, $uibModal, CategoryService) {
    /* Params */
    $scope.editMode = false;
    $scope.category = {};

    /* HTML Actions */
    $scope.submit = submit;
    $scope.removeCategory = removeCategory;

    /* Actions */
    function submit() {
        $scope.editMode ? CategoryService.saveCategory($scope.category).then(callUpdated) : CategoryService.createCategory($scope.category).then(clearForm);
    }

    function removeCategory() {
        if (!$scope.editMode) return;
        $uibModal.open({
            templateUrl: '/views/confirm_modal',
            resolve: {
                message: function() {
                    return 'Вы уверены, что хотите удалить категорию?';
                }
            },
            controller: function ($scope, $uibModalInstance, message) {
                $scope.message = message;
            }
        }).result.then(function() {
            CategoryService.removeCategory($scope.category).then(clearForm);
        })
    }

    function clearForm() {
        $location.path('/admin');
        callUpdated();
    }

    function callUpdated(){
        $rootScope.$emit('updated');
    }
    /* Initialization */
    function init() {
        $scope.editMode = $routeParams.id != 'create';
        if ($scope.editMode) {
            CategoryService.getCategory($routeParams.id).then(function(res) {
                $scope.category = res.data;
            });
        }
    }
    init();
}
