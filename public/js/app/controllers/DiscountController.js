DiscountController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', '$uibModal', 'DiscountService', 'CategoryService'];

function DiscountController($scope, $rootScope, $routeParams, $location, $uibModal, DiscountService, CategoryService) {
    /* Params */
    $scope.editMode = false;
    $scope.discount = {};
    $scope.newPlace = {};
    $scope.map = {
            center: {
                latitude: 53.9019481,
                longitude: 27.5688651
            },
            zoom: 11
        }
        /* HTML Actions */
    $scope.submit = submit;
    $scope.removeDiscount = removeDiscount;
    $scope.createPlace = createPlace;
    $scope.removePlace = removePlace;

    /* Actions */
    function submit() {
        $scope.editMode ? DiscountService.saveDiscount($scope.discount).then(callUpdated) : DiscountService.createDiscount($scope.discount).then(clearForm);
    }

    function removeDiscount() {
        if (!$scope.editMode) return;
        $uibModal.open({
            templateUrl: '/views/confirm_modal',
            resolve: {
                message: function() {
                    return 'Вы уверены, что хотите удалить скидку?';
                }
            },
            controller: function($scope, $uibModalInstance, message) {
                $scope.message = message;
            }
        }).result.then(function() {
            DiscountService.removeDiscount($scope.discount).then(clearForm);
        })
    }

    function createPlace() {
        if (!$scope.newPlace.address || $scope.newPlace.address.length < 1) return;
        if (!$scope.discount.places)
            $scope.discount.places = [];

        $scope.discount.places.push(angular.copy($scope.newPlace));
        $scope.newPlace = {};
    }

    function removePlace(indx) {
        $scope.discount.places.splice(indx, 1);
    }

    function clearForm() {
        $location.path('/admin');
        callUpdated();
    }
    function callUpdated(){
        $rootScope.$emit('updated');
    }
    /* Initialization */
    function init() {
        $scope.editMode = $routeParams.id != 'create';
        if ($scope.editMode) {
            DiscountService.getDiscount($routeParams.id).then(function(res) {
                $scope.discount = res.data;
            });
        }
        CategoryService.getCategories().then(function(res) {
            $scope.categories = res.data;
        })
    }
    init();
}
