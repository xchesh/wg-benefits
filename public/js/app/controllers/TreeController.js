TreeController.$inject = ['$scope', '$rootScope', '$q', 'DiscountService', 'CategoryService'];

function TreeController($scope, $rootScope, $q, DiscountService, CategoryService) {

    $scope.toggleCategory = toggleCategory;

    $rootScope.$on('updated', init);

    function toggleCategory(e, category) {
        if (e) {
            e.stopPropagation();
            e.preventDefault();
        }
        category._collapsed = !category._collapsed;
    }

    function init() {
        $q.all({
            discounts: DiscountService.getDiscounts(),
            categories: CategoryService.getCategories()
        }).then(setToScope);
    }

    function setToScope(res) {
        for (var p in res) {
            $scope[p] = res[p].data;
        }
    }
    init();
}
