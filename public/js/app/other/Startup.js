Startup.$inject = ['$rootScope', '$location'];

function Startup($rootScope, $location) {
    $rootScope.isPath = function(route){
        return $location.path().indexOf(route) >= 0;
    }
    $rootScope.isActive = function(route) {
        return $location.path() == route;
    }
}
