Router.$inject = ['$routeProvider'];

function Router($routeProvider) {
    $routeProvider.
    when('/', {
        templateUrl: '/views/app',
        controller: 'StartController',
        reloadOnSearch: false
    }).
    when('/admin', {
        templateUrl: '/views/admin_main',
        controller: 'AdminController',
        reloadOnSearch: false
    }).
    when('/admin/discounts/:id', {
        templateUrl: '/views/discount_view',
        controller: 'DiscountController',
        reloadOnSearch: false
    }).
    when('/admin/categories/:id', {
        templateUrl: '/views/category_view',
        controller: 'CategoryController',
        reloadOnSearch: false
    }).
    otherwise({
        redirectTo: '/',
        reloadOnSearch: false
    });
}
