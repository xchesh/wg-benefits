CategoryService.$inject = ['$http', '$q'];

function CategoryService($http, $q) {
    var service = this;
    service.getCategories = function() {
        return $http.get('/api/categories');
    }
    service.getCategory = function(id) {
        return $http.get('/api/categories/' + id);
    }
    service.createCategory = function(category) {
        return $http.post('/api/categories', category);
    }
    service.saveCategory = function(category) {
        return $http.put('/api/categories/' + category._id, category);
    }
    service.removeCategory = function(category) {
        return $http.delete('/api/categories/' + category._id);
    }
}
