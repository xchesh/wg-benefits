DiscountService.$inject = ['$http'];

function DiscountService($http) {
    var service = this;
    service.getDiscounts = function() {
        return $http.get('/api/discounts');
    };
    service.getDiscount = function(id) {
        return $http.get('/api/discounts/' + id);
    }
    service.createDiscount = function(discount) {
        return $http.post('/api/discounts', discount);
    }
    service.saveDiscount = function(discount) {
        return $http.put('/api/discounts/' + discount._id, discount);
    };
    service.removeDiscount = function(discount) {
        return $http.delete('/api/discounts/' + discount._id);
    };
}
