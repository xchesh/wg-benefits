var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DiscountSchema = new Schema({
    category_id: { type:String, default: 0 },
    title: String,
    img: String,
    description: String,
    short_description: String,
    places: [
        {
            coordinates: {
                latitude: String,
                longitude: String
            },
            address: String
        }
    ]
});

module.exports = mongoose.model('Discount', DiscountSchema);
